
// Loops

let myString = prompt("Enter a Name: ");

let myName

for (i=myString.length; i >= 0; i--){
	if (myString.length > 7 && myString[i] == 'a' || myString[i] == 'e' ||myString[i] == 'i' || myString[i] == 'o' || myString[i] == 'u') {
		console.log();
	} else {
		console.log(myString[i]);
	}

}



//  Array

let students = ["John", "Jane", "Joe"];

console.log("The total size of the array is "+students.length);

students.unshift("Jack");
console.log("Jack is added in the beginning of the array");
console.log(students);


students.push("Jill");
console.log("Jill is added in the end of the array");
console.log(students);


students.shift();
console.log("Jack was removed in the beginning of the array");
console.log(students);


students.pop();
console.log("Jill was removed in the end of the array");
console.log(students);



console.log("The last index of the array is: " + (students.length - 1));


students.forEach(function(student){
	console.log(student);
})

students.splice(1, 1)
console.log(students)

//Object

let person = {
	firstName: "Juan",
	lastName: "Dela Cruz",
	nickName: "J",
	age: 25,
	address:{
		city: "Quezon City",
		country: "Philippines"
	},
	friends: ["John", "Jane", "Joe"]
};

console.log("My name is "+person.firstName+' '+person.lastName+", but you can call me "+person.nickName+". I am "+person.age+" years old and I live in "+ person.address.city+', '+person.address.country+". My friends are "+person.friends[0]+', '+person.friends[1]+', '+person.friends[2]+'.');



// ES6 Update

console.log(`My Name is ${person.firstName} ${person.lastName}, but you can call me ${person.nickName}. I am ${person.age} years old and I live in ${person.address.city}, ${person.address.country}. My friends are ${person.friends[0]} ${person.friends[1]} and ${person.friends[2]}.`)







